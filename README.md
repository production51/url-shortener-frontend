# URL Shortener - Frontend

This project is a simple interface written in Flutter/Dart to communicate with the URL Shortener Backend API.

# Heroku
Note that Heroku does not support Flutter/Dart applications by default and a Procfile needs to be defined in order to deploy this app.