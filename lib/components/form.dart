import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:url_shortener_frontend/domain/PostResponse.dart';

// Define a custom Form widget.
class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key});

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Define a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a `GlobalKey<FormState>`,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();
  final controller = TextEditingController();
  PostResponse? response;

  Future<PostResponse> postRequest(String longUrl) async {
    print("sending request");
    return http.post(
      Uri.parse('https://marc-brun-url-shortener-go.herokuapp.com/'),
      body: <String, String>{"url": longUrl},
    ).then((response) {
      print("received response");
      return PostResponse.fromJson(jsonDecode(response.body));
    });
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Text(
            'Enter a URL to shorten:',
            style: Theme.of(context).textTheme.headlineLarge,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 48, vertical: 24),
            child: TextFormField(
              controller: controller,
              // The validator receives the text that the user has entered.
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
            ),
          ),
          ElevatedButton(
            onPressed: () {
              // Validate returns true if the form is valid, or false otherwise.
              if (_formKey.currentState!.validate()) {
                postRequest(controller.text).then((response) {
                  setState(() {
                    this.response = response;
                  });
                });
                // If the form is valid, display a snackbar.
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Processing Data')),
                );
              }
            },
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 8),
              child: Text(
                'Shorten URL',
                style: TextStyle(fontSize: Theme.of(context).textTheme.headlineSmall?.fontSize),
              ),
            ),
          ),
          if (response != null)
            Padding(
              padding: EdgeInsetsDirectional.all(48),
              child: response?.shortUrl != null
                  ? SelectableText(
                      "Generated short URL: " + (response?.shortUrl)!,
                      style: Theme.of(context).textTheme.headlineSmall,
                    )
                  : response?.errorMessage != null
                      ? Text(
                          "Could not generate short URL: " + (response?.errorMessage)!,
                          style: Theme.of(context).textTheme.headlineSmall?.copyWith(color: Theme.of(context).errorColor),
                        )
                      : null,
            )
        ],
      ),
    );
  }
}
