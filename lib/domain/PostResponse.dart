class PostResponse {
  final String? shortUrl;
  final String? errorMessage;

  const PostResponse({
    required this.shortUrl,
    required this.errorMessage,
  });

  factory PostResponse.fromJson(Map<String, dynamic> json) {
    return PostResponse(
      shortUrl: json['url'],
      errorMessage: json['error'],
    );
  }
}
