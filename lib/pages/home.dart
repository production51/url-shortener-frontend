import 'package:flutter/material.dart';
import 'package:url_shortener_frontend/components/form.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, this.title});

  final String? title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title!,
          style: TextStyle(fontSize: Theme.of(context).textTheme.headlineLarge?.fontSize),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 100),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const MyCustomForm(),
            ],
          ),
        ),
      ),
    );
  }
}
